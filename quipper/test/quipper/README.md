To run the tests just type:
```
$>lein midje
```
But it might be useful to use the autotest functionality.  This keeps the same jvm alive and re-runs tests as files change, saving you the time it takes to when you run 'lein midje'.  Do this by including the ':autotest' argument.
```
$>lein midje :autotest
```
More information can be found:

https://github.com/marick/Midje/wiki/Autotest