# quipper

Quipper will let you manage your quips.  You can add quips to your quips database, retrieve random quips, and get a count of how many quips you've collected.  If you find yourself feeling especially nihilistic you can even drop all your quips and start over.

## Usage

>    $> java -jar quipper-0.1.0-standalone.jar [args]

or, if you're in development mode

>    $> lein run -- [args]

## Options

    -h,--help Prints the help information
    -f,--file provides an alternate file for quip storage.
        (used in testing but you can use it, too)

## Actions

* add   - Adds one or more quip
* get   - Gets a random quip
* count - Counts the quips in the data base and prints the value to the screen.
* drop  - Drops the current database so you can start over.
          Sometimes you just want to watch it all burn.

## Examples

#### Add some quips
```

$> lein run -- add "If at first you don't succeed, so much for skydiving."
$> lein run -- add "I'm a cake man.\n--Anh" \
    "Two guys walk into a bar.  You'd think the second one would have ducked!"

```

#### See how many quips you've added
```

$> lein run -- count
3
$>

```
#### Get a random quip

```
$> lein run -- get
I'm a cake man.
--Anh
$> lien run
If at first you don't succeed, so much for skydiving.
$>

```
### Bugs

This code is completely bug free.  As far as I know.
